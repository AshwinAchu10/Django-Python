from django.db import models
from django.core.urlresolvers import reverse


class Employee(models.Model):
    name = models.CharField(max_length=20)
    uniqueid = models.IntegerField()
    age = models.IntegerField()
    language = models.CharField(max_length=20)
    department = models.CharField(max_length=20)

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('employee:edit', kwargs={'pk': self.pk})