from django.shortcuts import render, redirect, get_object_or_404
from django.forms import ModelForm

from employee.models import Employee

class EmployeeForm(ModelForm):
    class Meta:
        model = Employee
        fields = ['name', 'uniqueid', 'age', 'language','department']

def list(request, template_name='employee/list.html'):
    emp = Employee.objects.all()
    data = {}
    data['object_list'] = emp
    return render(request, template_name, data)

def create(request, template_name='employee/form.html'):
    form = EmployeeForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('employee:list')
    return render(request, template_name, {'form':form})

def update(request, pk, template_name='employee/form.html'):
    emp= get_object_or_404(Employee, pk=pk)
    form = EmployeeForm(request.POST or None, instance=emp)
    if form.is_valid():
        form.save()
        return redirect('employee:list')
    return render(request, template_name, {'form':form})

def delete(request, pk, template_name='employee/delete.html'):
    emp= get_object_or_404(Employee, pk=pk)    
    if request.method=='POST':
        emp.delete()
        return redirect('employee:list')
    return render(request, template_name, {'object':emp})
