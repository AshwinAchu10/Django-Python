from django.conf.urls import patterns, url

from employee import views

urlpatterns = patterns('',
  url(r'^$', views.list, name='list'),
  url(r'^new$', views.create, name='new'),
  url(r'^edit/(?P<pk>\d+)$', views.update, name='edit'),
  url(r'^delete/(?P<pk>\d+)$', views.delete, name='delete'),
)