from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import TemplateView
from person.templates import *

urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name="index.html"), name='index'),
    url(r'^polls/', include('polls.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^custom/', include('person.urls')),
    url(r'^users/', include('users.urls')),

]