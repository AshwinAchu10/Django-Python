from django.db import models
from django.utils import timezone
from polls.models import Question
import datetime
from django.contrib.auth.models import AbstractUser


class Person(models.Model):
    name = models.CharField(max_length=200,blank=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

class College(models.Model):
    collegeName = models.CharField(max_length=200)
    persons = models.OneToOneField(Person,blank=True)

    def __str__(self):
        return self.collegeName

class Department(models.Model):
    dept = models.CharField(max_length=30)
    person=models.ManyToManyField(Person,blank=True)

    def __str__(self):
        return self.dept

class Books(models.Model):
    book = models.CharField(max_length=30)
    dept = models.ForeignKey(Department,blank=True)

    def __str__(self):
        return self.book




