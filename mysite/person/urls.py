from django.conf.urls import url
from django.views.generic.base import TemplateView

from person import views

app_name = 'person'
urlpatterns = [
    url(r'^personlist/$', views.list, name='plist'),
    url(r'^createperson/$', views.create, name='pnew'),
    url(r'^collegelist/$', views.clist, name='clist'),
    url(r'^createcollege/$', views.ccreate, name='cnew'),
    url(r'^deptlist/$', views.dlist, name='dlist'),
    url(r'^createdept/$', views.dcreate, name='dnew'),
    url(r'^booklist/$', views.blist, name='blist'),
    url(r'^createbook/$', views.bcreate, name='bnew'),
]