from django.shortcuts import get_object_or_404, render
from django.shortcuts import *
from django.utils import timezone
from .models import Question
from django.contrib import messages
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views import generic
from person.models import Person,College,Department,Books
from django.forms import ModelForm

class Form(ModelForm):
    class Meta:
        model = Person
        fields = ['name']
class CForm(ModelForm):
    class Meta:
        model = College
        fields = ['collegeName','persons']
class DForm(ModelForm):
    class Meta:
        model = Department
        fields = ['dept','person']
class BForm(ModelForm):
    class Meta:
        model = Books
        fields = ['book','dept']

def create(request, template_name='person/create.html'):
    form = Form(request.POST or None)
    if form.is_valid():
        form.save()
        messages.success(request,  'Person Created')
        return redirect('person:plist')
    return render(request, template_name, {'form': form})

def list(request, template_name='person/list.html'):
    p = Person.objects.all()
    data = {}
    data['object_list'] = p
    return render(request, template_name, data)

def ccreate(request, template_name='college/create.html'):
    cform = CForm(request.POST or None)
    if cform.is_valid():
        cform.save()
        messages.success(request, 'College Created')
        return redirect('person:clist')
    return render(request, template_name, {'form': cform})

def clist(request, template_name='college/list.html'):
    c = College.objects.all()
    data = {}
    data['object_list'] = c
    return render(request, template_name, data)

def dcreate(request, template_name='dept/create.html'):
    dform = DForm(request.POST or None)
    if dform.is_valid():
        dform.save()
        messages.success(request, 'Department Created')
        return redirect('person:dlist')
    return render(request, template_name, {'form': dform})

def dlist(request, template_name='dept/list.html'):
    d = Department.objects.all()
    data = {}
    data['object_list'] = d
    return render(request, template_name, data)

def bcreate(request, template_name='books/create.html'):
    bform = BForm(request.POST or None)
    if bform.is_valid():
        bform.save()
        messages.success(request, 'Book Created')
        return redirect('person:blist')
    return render(request, template_name, {'form': bform})

def blist(request, template_name='books/list.html'):
    b = Books.objects.all()
    data = {
    }
    data['object_list'] = b
    return render(request, template_name, data)
