from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.
class UserProfile(AbstractUser):
    """
    User model
    """
    sex = models.CharField(max_length=2, blank=True) # we will store the sex here
    birthdate = models.DateField(null=True, blank=True)
    phone = models.CharField(max_length=32,blank=True,null=True)
