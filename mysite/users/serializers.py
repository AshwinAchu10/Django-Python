from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import get_object_or_404
from rest_framework import serializers, exceptions
from rest_framework.authtoken.models import Token

from django.shortcuts import get_object_or_404
from django.contrib.auth import authenticate
from django.core.urlresolvers import reverse
from django.conf import settings

from django.utils import timezone
from datetime import datetime
from rest_framework import serializers
from rest_framework.authtoken.models import Token
from users.models import UserProfile


class UserRegistrationSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserProfile
        fields = [
            'username',
            'email',
            'password',
            'first_name',
            'last_name',
            'date_joined',
            'phone',
            'sex',
            'birthdate',
        ]
        extra_kwargs = {
            'password': {'write_only': True, 'required': False},
            'date_joined': {'read_only': True},
            'username': {'required': False},
        }
    def create(self, validated_data):
        user = UserProfile(**validated_data)
        if not validated_data.get('password'):
            msg = '"password" is required to create a user.'
            raise exceptions.ValidationError(msg)
        password = validated_data.get('password')
        if password:
            user.set_password(password)
        if not user.username and not validated_data.get('username'):
            pass
        user.save()
        return user





class TokenSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()

    def validate(self, attrs):
        username = attrs.get('username')
        password = attrs.get('password')
        last_login=timezone.now()

        if username and password:
            u = get_object_or_404(UserProfile,username=username)
            user = authenticate(username=u.username, password=password)
            if user:
                if not user.is_active:
                    msg = 'User account is disabled.'
                    raise exceptions.ValidationError(msg)
            else:
                msg = 'Unable to log in with provided credentials.'
                raise exceptions.ValidationError(msg)
        else:
            msg = '"username" and "password" are required.'
            raise exceptions.ValidationError(msg)
        attrs['last_login'] = last_login
        attrs['user'] = user
        user.save()
        return attrs