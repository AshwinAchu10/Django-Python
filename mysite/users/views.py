from rest_framework import status
from django.shortcuts import get_object_or_404
from rest_framework.authtoken.models import Token
from rest_framework.generics import CreateAPIView, GenericAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from users.serializers import UserRegistrationSerializer, TokenSerializer
from django.http import JsonResponse
from django.contrib import auth

from django.core.exceptions import FieldError
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework import views, viewsets, generics, filters
from rest_framework import parsers, renderers, exceptions
from rest_framework.permissions import AllowAny

from users.serializers import *
from users.models import UserProfile

from django.utils import timezone

class UserRegistrationAPIView(CreateAPIView):
    authentication_classes = ()
    permission_classes = ()
    serializer_class = UserRegistrationSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)

        user = serializer.instance
        token, created = Token.objects.get_or_create(user=user)
        data = serializer.data
        data["token"] = token.key

        headers = self.get_success_headers(serializer.data)
        return Response(data, status=status.HTTP_201_CREATED, headers=headers)


class UserLogoutAPIView(APIView):

    def post(self, request, *args, **kwargs):
        Token.objects.filter(user=request.user).delete()
        return Response(status=status.HTTP_200_OK)

class TokenAPIView(views.APIView):
    throttle_classes = ()
    permission_classes = ()
    parser_classes = (
        parsers.FormParser,
        parsers.MultiPartParser,
        parsers.JSONParser,
    )

    renderer_classes = (renderers.JSONRenderer,)

    def post(self, request):
        last_login=timezone.now()
        serializer = TokenSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)

        content = {
            'last_login':last_login,
            'token':str(token.key),
        }
        return Response(content)

    def get(self, request):
        user = request.user
        if not request.user.is_authenticated():
            raise exceptions.NotFound()
        token, created = Token.objects.get_or_create(user=user)

        content = {
            'token':str(token.key),
        }
        return Response(content)