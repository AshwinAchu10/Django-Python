from django.conf.urls import url
from users.views import UserRegistrationAPIView, UserLogoutAPIView, TokenAPIView
app_name = 'users'
urlpatterns = [
    url(r'^$', UserRegistrationAPIView.as_view(), name="list"),
    url(r'^token/$', TokenAPIView.as_view()),
    url(r'^logout/$', UserLogoutAPIView.as_view(), name="logout"),
]